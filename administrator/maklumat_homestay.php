<?PHP 
#memanggil fail header_admin.php
include ('header_admin.php');
#memanggil fail connection dari folder luaran
include ('../connection.php');
#--------------------Bahagian 2 :Proses penyimpan data-----------
#menyemak kewujudan data POST
if(!empty($_POST))
{
	#mengambil data POST
	$id_Rumah=$_POST['id_Rumah'];
	$Alamat=$_POST['Alamat'];
	$id_Jenis=$_POST['id_Jenis'];
	$Harga=$_POST['Harga'];
	#memproses maklumat gambar yang diupload
	#proses ini lebih kepada menukar nama fail gambar
	$timestmp=date("Y-m-d");
	$saiz_fail = $_FILES['Gambar']['size'];
	$nama_fail = basename($_FILES["Gambar"]["name"]);
	$jenis_gambar = pathinfo($nama_fail,PATHINFO_EXTENSION);
	$lokasi = $_FILES['Gambar']['tmp_name'];
	$folder = "../images/";
	$nama_baru_gambar=$id_Rumah.$timestmp.".".$jenis_gambar;
	#arahan untuk menyimpan data ke dalam jadual homestay
	$arahan_sql_simpan="insert into homestay
	value
	('$id_Rumah','$Alamat','$id_Jenis','$Harga','$nama_baru_gambar')";
	#melaksanakan proses menyimpan data syarat IF
	if(mysqli_query($condb,$arahan_sql_simpan))
	{
		move_uploaded_file($lokasi,$folder.$nama_baru_gambar);
		#proses menyimpan data berjaya. papar mesej
		echo "<script>alert('Pendaftaran BERJAYA');
		window.location.href='maklumat_homestay.php';</script>";
	}
	else
	{
		#proses menyimpan data gagal. papar mesej
		echo "<script>alert('Pendaftaran GAGAL');
		window.history.back();</script>";
	}
}
#-------------bahagian 1 :memaparkan data dalam bentuk jadual
#arahan sql mencari data homestay
$arahan_sql_cari="select* from rumah,jenis_rumah where rumah.id_Jenis= jenis_rumah.id_Jenis";
#melaksanakan arahan sql cari tesebut
$laksana_sql_cari=mysqli_query($condb,$arahan_sql_cari);
?>
<!--menyediakan header bagi jadual-->
<!--selepas header akan diselitkan dengan borang untuk mendaftar homestay baru -->
<h4>Senarai Homestay Berdaftar</h4>
<table id='saiz' boarder='1'>
	<tr>
		<td>Bil</td>
		<td>id_Rumah</td>
		<td>Alamat</td>
		<td>Jenis Rumah</td>
		<td>Harga</td>
		<td>Gambar</td>
		<td></td>
	</tr>
	<tr>
		<!--menyediakan borang untuk mendaftar homestay baru-->
		<form action='' method='POST' enctype='multipart/form-data'>
			<td>#</td>
			<td><input type='text' name='id_Rumah'></td>
			<td><input type='text' name='Alamat'></td>
			<td>
				<!--menghasilkan drop down yang dinamik (mengambil data dari jadual jenis_rumah)-->
				<select name='id_Jenis' required>
				<option disabled selected value>Pilih kategori</option>
				<?PHP 
				#arahan mencari data dari jadual jenis_rumah
				$arahan_sql_carijenis= "SELECT* from jenis_rumah";
				#melaksanakan arahan mencari tersebut
				$laksanakan_sql_carijenis=mysqli_query($condb,$arahan_sql_carijenis);
				#pembolehubah $rekod_model mengambil data baris demi baris
				while($rekod_jenis=mysqli_fetch_array($laksana_sql_carijenis))
				{
					#memaparkan nilai pembolehubah $rekod_jenis['jenis'] dalam bentuk dropdown list
					echo"<option value='".$rekod_jenis['id_Jenis']."'>
					".$rekod_jenis['Jenis']."</option>"
				}
				?>
				</select>
			</td>
			<td><input type='text' name='Harga'></td>
			<td><input type='file' name='Gambar'></td>
			<td><input type='submit' name='simpan'></td>
			</form>
	</tr>
	<?PHP 
		$bil=0;
		#pemboleh ubah $rekod mengambil semua data yang ditemui oleh $laksana_sql_cari
		while ($rekod=mysqli_fetch_array($laksana_sql_cari))
		{
			#sistem akan memaparkan data $rekod baris demi baris sehingga habis
			echo "
			<tr>
				<td>".++$bil."</td>
				<td>".$rekod['id_Rumah']."</td>
				<td>".$rekod['Alamat']."</td>
				<td>".$rekod['Jenis']."</td>
				<td>".$rekod['Harga']."</td>
				<td><img src='../images/".$rekod['Gambar']."' width='10%'></td>
				<td>
					<a href='hapus.php?jadual=homestay&medan_id=id_Rumah&id=".$rekod['id_Rumah']."'
					onClick=\"return confirm('Anda pasti ingin padam data ini?')\" >Hapus</a></td>
				</tr>";
		}
		?>
	</table>
