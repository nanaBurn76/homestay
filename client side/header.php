<?PHP
# Mulakan fungsi Session
session_start();
date_default_timezone_set("Asia/Kuala_Lumpur");
#--------Bahagian login & logout Session------=

$namafail = basename($_SERVER['PHP_SELF']);

if(($namafail !='index.php' and $namafail !='tempahan_bayar.php' and $namafail !='tempahan_senarai.php' and $namafail !='pelanggan_daftar.php' and 
$namafail !='pelanggan_login.php') and empty($_SESSION['nama_pelanggan']))
{
	die("<script>alert('Sila daftar Masuk');window.location.href='logout.php';</script>");
}
?>

<!--tajuk sistem-->
<h2>Sistem Tempahan Homestay Jaga</h2>
<hr>

<!--bahagian menu utama. Boleh diakses oleh pengguna yang telah login dan belum login-->
<a href="index.php">Laman utama</a>

<?PHP if(empty($_SESSION["nama_Pelanggan"])){ ?>

<!--bahagian menu yang boleh diakses oleh pengguna yang belum login -->
<a href="pelanggan_login.php">Daftar masuk</a>
<a href="pelanggan_daftar.php">Daftar Pembeli baru</a>

<?PHP } else{ ?>

<!--bahagian menu yang boleh diakses oleh pengguna selepas login-->
	<a href="logout.php">logout</a>	

<?PHP } ?>

<hr>
